{-# LANGUAGE BangPatterns      #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Control.Concurrent         (forkIO, myThreadId)
import Control.Concurrent.MVar
import Data.Conduit               (ConduitT, ConduitM, await, awaitForever, runConduit, (.|))
import Debug.Trace                
import GHC.Conc                   (labelThread)
import System.Mem                 
import Control.Parallel.Strategies (rdeepseq, usingIO)
import Data.Aeson (decodeStrict, Value)
import System.IO
import System.Exit
import Control.Monad.IO.Class
import Control.Monad
import Control.Exception
import Control.Concurrent.Async

import qualified Control.Concurrent.STM          as STM
import qualified Data.Conduit.Combinators        as CC
import qualified Data.Vector                     as V
import qualified Data.ByteString as B

go2
  :: (MonadIO m)
  => MVar ()
  -> (ConduitT () () m () -> IO z)
  -> IO z
go2 exitMVar go = do
  (writeMsg, readMsg) <- parse

  let notifyOfNewMessage message = do
        liftIO $ traceEventIO $ "notify"
        writeMsg message

      clientThread = withFile "./data" ReadMode $ clientThreadGo
      clientThreadGo h = do 
       B.hGetLine h >>= notifyOfNewMessage 
       eof <- hIsEOF h 
       if eof then putMVar exitMVar () else clientThreadGo h

  withAsync clientThread $ \_ -> 
    go (CC.repeatM (liftIO $ traceEventIO "readMsg" >> readMsg) .| applyConduit)

applyConduit
  :: (Monad m, MonadIO m)
  => ConduitM ((Maybe Value)) () m ()
applyConduit = go
  where
  go = await >>= \case
    Nothing -> do
      liftIO $ traceEventIO "unexpected exit"
      liftIO performMajorGC
    Just aed -> do
      void $ liftIO $ usingIO aed rdeepseq
      liftIO performMajorGC
      liftIO $ traceEventIO "rec"
      go 

parse :: IO (B.ByteString -> IO (), IO (Maybe Value))
parse = mapMPara "parsePara" 512 $ f
  where
  f (rawEventContent) = do
    liftIO $ traceEventIO $ "time"
    let eventContent = decodeStrict rawEventContent
    void $ usingIO eventContent rdeepseq
    case eventContent of
      Nothing -> traceM "could not decode json" >> pure Nothing
      Just ied -> pure $ Just ied

mapMPara :: String -> Int -> (a -> IO b) -> IO (a -> IO (), IO b)
mapMPara label width f = do
  (ins, outs) <- liftIO $ do
    ins <- V.replicateM width newEmptyMVar
    outs <- V.replicateM width newEmptyMVar
    let
      setupThread n = do
        mtid <- myThreadId
        labelThread mtid (label ++ show n)
        mIn <- evaluate $ V.unsafeIndex ins n
        mOut <- evaluate $ V.unsafeIndex outs n
        forever (takeMVar mIn >>= \x -> f x >>= putMVar mOut)
    mapM_ (void . forkIO . setupThread) [0..width-1] 
    pure (ins, outs)
  writeCounter <- newMVar 0
  readCounter <- newMVar 0
  let inc arr ref = modifyMVar ref (\x -> pure ((x+1) `mod` width, V.unsafeIndex arr x))
  let write newVal = do
        y <- inc ins writeCounter
        liftIO $ putMVar y newVal
  let read = do
        y <- inc outs readCounter
        liftIO $ takeMVar y
  pure (write, read)


main :: IO ()
main = do
  let 
    streamAndApply source = source .| awaitForever (const $ pure ())
  exitMVar <- newEmptyMVar
  forkIO $ go2 exitMVar $ runConduit . streamAndApply
  takeMVar exitMVar
  exitSuccess
