let
  pkgs = import (builtins.fetchTarball
    "https://github.com/NixOS/nixpkgs/archive/31ffc50c571e6683e9ecc9dbcbd4a8e9914b4497.tar.gz") {
      overlays = [
        (final: prev: {
          llvm_39 = final.llvm;
          stdenv = prev.stdenv // { inherit (prev) lib; };
        })
      ];
    };

  mkGhc = (import (pkgs.fetchFromGitHub {
    owner = "mpickering";
    repo = "old-ghc-nix";
    rev = "11837e907c04e941a92381fd74e404f20b772e27";
    sha256 = "0jfiqkfl7lk9xn4cbhs5sjn6nzqncffvylihrbr2p00my4zyy6nd";
  }) { inherit pkgs; }).mkGhc;

  hps = (pkgs.haskellPackages.extend (self: super: rec {
    mkDerivation = args: super.mkDerivation (args // { doCheck = false; });
  }));

  ghc = mkGhc {
    url =
      "https://gitlab.haskell.org/ghc/ghc/-/jobs/782803/artifacts/raw/ghc-x86_64-fedora27-linux.tar.xz";
    hash = "1pmjhc2qa5fs0c3x30jhslr81nr7fb79ygfc06zn2csqsg747ap7";
  };

in pkgs.mkShell {
  buildInputs = [
    ghc
    pkgs.cabal-install
    pkgs.ncurses
    pkgs.wget
    pkgs.curl
    pkgs.zlib
    pkgs.elfutils
    pkgs.git
    pkgs.gmp
  ];
}
